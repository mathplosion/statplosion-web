# statplosion-web

This is a web app for statplosion. It is built with Vuejs, Fastapi, and Bulma.

Uses Vuejs, Vuex, VueRouter from the CDN 

Create the virtual environment, activate it, and install the requirements:

>python3 -m venv env

>source env/bin/activate

>python3 -m pip install -r requirements.txt

