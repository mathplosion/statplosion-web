#!/bin/bash
[ -f "problems.db" ] && rm "problems.db"
sqlite3 -echo problems.db < problems.sql
uvicorn main:app
