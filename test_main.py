from fastapi.testclient import TestClient
import pytest
import os

from main import app
client = TestClient(app)

@pytest.fixture(scope="function")
def setup():
    """ Fixture to set up the database with test data """
    # print("setting up database")
    os.system("./makedb.sh")

def test_read_main(setup):
    response = client.get("/api/v1")
    assert response.status_code == 200
    assert response.json() == {"Hello": "Fastapi"}

def test_get_problems(setup):
    response = client.get("/api/v1/lists/1/problems/")
    assert response.status_code == 200
    assert response.json() == [
        { "id": 1, "name": "p1", "question": "q1", "answer": "a1", "position": 0, "listid": 1},
        { "id": 2, "name": "p2", "question": "q2", "answer": "a2", "position": 1, "listid": 1},
        { "id": 3, "name": "p3", "question": "q3", "answer": "a3", "position": 2, "listid": 1}
    ]

def test_get_problem(setup):
    response = client.get("/api/v1/problems/2")
    assert response.status_code == 200
    assert response.json() == {"id": 2, 
                                "name": "p2", 
                                "question": "q2", 
                                "answer": "a2", 
                                "position": 1,
                                "listid": 1} 

def test_update_problem(setup):
    response = client.put("/api/v1/problems/1", 
               json={ "name": "problem1", 
               "question": "q1", 
               "answer": "a1"})
    assert response.status_code == 200
    assert response.json() == {"id": 1, 
               "name": "problem1", 
               "question": "q1", 
               "answer": "a1",
               "position": 0,
               "listid": 1}

def test_delete_problem(setup):
    response = client.delete("/api/v1/problems/1")
    assert response.status_code == 200
    assert response.json() == {}

def test_append_problem(setup):
    response = client.post("/api/v1/lists/1/problems/", 
                            json={"name": "p4", 
                                  "question": "q4", 
                                  "answer": "a4"})

    problem = response.json()
    assert response.status_code == 200
    assert problem['id'] == 6 
    assert problem['name'] == "p4" 
    assert problem['question'] == "q4"
    assert problem['answer'] == "a4"
    assert problem['position'] == 3 
    assert problem['listid'] == 1 

