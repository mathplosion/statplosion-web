#!/bin/bash
# remove and recreate any problems.db 
[ -f "problems.db" ] && rm "problems.db"
sqlite3 problems.db < problems.sql

echo "GET root"
curl -X GET "http://localhost:8000/api" -H "accept: application/json" -w "\n\n"

echo "GET problems"
curl -X GET "http://localhost:8000/api/problems/" -H  "accept: application/json" -w "\n\n"

echo "GET problems/2" 
curl -X GET "http://localhost:8000/api/problems/2" -H  "accept: application/json" -w "\n\n"

echo "PUT problems/1"
curl -X PUT "http://localhost:8000/api/problems/1" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"id\":1,\"name\":\"p1new\",\"question\":\"q1new\",\"answer\":\"a1new\",\"position\":2}" -w "\n\n"

echo "GET problems/1" 
curl -X GET "http://localhost:8000/api/problems/1" -H  "accept: application/json" -w "\n\n"

echo "DELETE problems/1"
curl -X DELETE "http://localhost:8000/api/problems/1" -H  "accept: application/json" -w "\n\n"

echo "GET problems"
curl -X GET "http://localhost:8000/api/problems/" -H  "accept: application/json" -w "\n\n"

echo "POST problems"
curl -X POST "http://localhost:8000/api/problems/" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"id\":4,\"name\":\"p4\",\"question\":\"q4\",\"answer\":\"a4\",\"position\":4}" -w "\n\n"

echo "GET problems"
curl -X GET "http://localhost:8000/api/problems/" -H  "accept: application/json" -w "\n\n"

# # remove and recreate any problems.db 
# [ -f "problems.db" ] && rm "problems.db"
# sqlite3 problems.db < problems.sql
