import pytest
import os
from database import Problem, List
from database import get_problems, get_problem
from database import get_lists, get_list
from database import update_problem, append_problem
from database import delete_problem_by_index, delete_problem_by_id

def problem_equal(p1,p2):
    assert p1.id == p2.id 
    assert p1.name == p2.name
    assert p1.question == p2.question
    assert p1.answer == p2.answer
    assert p1.position == p2.position

def make_problem(i,n,q,a,p):
      return Problem(id=i, 
                     name=n, 
                     question=q, 
                     answer=a, 
                     position=p)

@pytest.fixture(scope="function")
def setup():
    """ Fixture to set up the database with test data """
    # print("setting up database")
    os.system("./makedb.sh")

def test_get_lists(setup):
    lists = get_lists()
    assert len(lists) == 2 

def test_get_list(setup):
    list1 = get_list(id=1)
    problems1 = get_problems(listid=list1.id)
    assert len(problems1) == 3

    list2 = get_list(id=2)
    problems2 = get_problems(listid=list2.id)
    assert len(problems2) == 2 

def test_delete_problem_by_index(setup):
    problem = delete_problem_by_index(listid=1,index=0)
    assert problem == {}

    problems = get_problems(listid=1)
    assert len(problems) == 2

    problem_equal(problems[0], make_problem(2,"p2","q2","a2",0))
    problem_equal(problems[1], make_problem(3,"p3","q3","a3",1)) 


def test_get_problems(setup):
    problems = get_problems(listid=1)

    assert len(problems) == 3 

    problem_equal(problems[0], make_problem(1,"p1","q1","a1",0)) 
    problem_equal(problems[1], make_problem(2,"p2","q2","a2",1))
    problem_equal(problems[2], make_problem(3,"p3","q3","a3",2))

def test_get_problem(setup):
    returned = get_problem(id=2)
    problem_equal(returned, make_problem(2,"p2","q2","a2",1))

def test_update_problem(setup):
    update=Problem(id=1, name="problem1", question="q1", answer="a1", position=0)

    returned = update_problem(update.id,update)

    problem_equal(returned, make_problem(1,"problem1","q1","a1",0)) 


def test_delete_problem_by_id(setup):
    returned = delete_problem_by_id(id=1)
    assert returned == {}
    problems = get_problems(listid=1)
    assert len(problems) == 2 

    problem_equal(problems[0], make_problem(2,"p2","q2","a2",0))
    problem_equal(problems[1], make_problem(3,"p3","q3","a3",1)) 


def test_append_problem(setup):

    problem=Problem(name="p6", question="q6", answer="a6")
    returned = append_problem(listid=1, problem=problem)

    problem_equal(returned, make_problem(6,"p6","q6","a6", 3))

    problems = get_problems(listid=1)
    assert len(problems) == 4 

    problem_equal(problems[0], make_problem(1,"p1","q1","a1",0))
    problem_equal(problems[1], make_problem(2,"p2","q2","a2",1))
    problem_equal(problems[2], make_problem(3,"p3","q3","a3",2))
    problem_equal(problems[3], make_problem(6,"p6","q6","a6",3))


