from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship 
from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy import create_engine
from sqlalchemy.ext.orderinglist import ordering_list
from contextlib import contextmanager
import os
import logging

logging.basicConfig(format="%(levelname)s ------- %(message)s")
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

Base = declarative_base()

class List(Base):
    __tablename__ = 'lists'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    problems = relationship("Problem", 
                             back_populates="list", 
                             order_by="Problem.position",
                             collection_class=ordering_list("position"))

    def __repr__(self):
        return "list (%s %s)" % (self.id, self.name)

class Problem(Base):
    __tablename__ = 'problems'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    question = Column(Text)
    answer = Column(Text)
    position = Column(Integer)
    listid = Column(Integer, ForeignKey('lists.id'))
    list = relationship("List", 
                         back_populates="problems")

    def __repr__(self):
        return "problem (%s %s %s %s %s %s)" % (self.id, 
                                             self.name, 
                                             self.question,
                                             self.answer,
                                             self.position, 
                                             self.listid)


engine = create_engine(
    "sqlite:///problems.db", 
    connect_args={"check_same_thread": True}
)


Session = sessionmaker(bind=engine)
Base.metadata.create_all(bind=engine)

# a scoped session which just closes
@contextmanager
def scoped_session():
    session = Session()
    try:
        yield session
    finally:
        session.close()

def get_problems(listid):
    with scoped_session() as session:
        list = session.query(List).get(listid)
        return list.problems

def get_lists():
    with scoped_session() as session:
        return session.query(List).all()

def get_list(id):
    with scoped_session() as session:
        return session.query(List).get(id)

def delete_problem_by_id(id):
    with scoped_session() as session:
        problem = session.query(Problem).get(id)
        problem.list.problems.remove(problem)
        session.delete(problem)
        session.commit()
        return {}

def delete_problem_by_index(listid, index):
    with scoped_session() as session:
        list = session.query(List).get(listid)
        problem=list.problems.pop(index)
        session.delete(problem)
        session.commit()
        return {}

def get_problem(id):
    with scoped_session() as session:
        return session.query(Problem).get(id)

def update_problem(id, problem):
    with scoped_session() as session:
        p = session.query(Problem).get(id)

        p.name = problem.name
        p.question = problem.question
        p.answer = problem.answer
        #p.position = problem.position

        session.commit()

        return session.query(Problem).get(id)

def append_problem(listid, problem):
    with scoped_session() as session:
        p = Problem(name = problem.name,
                    question = problem.question,
                    answer= problem.answer)
                    # position = problem.position)
        list = session.query(List).get(listid)
        list.problems.append(p)
        session.commit()
	# the new id is on p now
        return session.query(Problem).get(p.id)

