CREATE TABLE lists (
        id INTEGER NOT NULL, 
        name VARCHAR(250), 
        PRIMARY KEY (id)
);
CREATE TABLE problems (
        id INTEGER NOT NULL,
        name VARCHAR(250),
        question TEXT,
        answer TEXT,
        position INTEGER,
        listid INTEGER,
        PRIMARY KEY (id),
        FOREIGN KEY(listid) REFERENCES list (id)
);
INSERT INTO lists (name) VALUES("list1");
INSERT INTO problems (name, question, answer, position, listid) 
    VALUES("p1","q1","a1", 0, 1);
INSERT INTO problems (name, question, answer, position, listid) 
    VALUES("p2","q2","a2", 1, 1);
INSERT INTO problems (name, question, answer, position, listid) 
    VALUES("p3","q3","a3", 2, 1);

INSERT INTO lists (name) VALUES("list2");
INSERT INTO problems (name, question, answer, position, listid) 
    VALUES("p5","q5","a5", 0, 2);
INSERT INTO problems (name, question, answer, position, listid) 
    VALUES("p6","q6","a6", 1, 2);
