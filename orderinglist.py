from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy import create_engine

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.orderinglist import ordering_list
from sqlalchemy.orm import sessionmaker, relationship 

# from sqlalchemy.orm import backref 
import os

import logging
logging.basicConfig(format="----%(message)s")
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

Base = declarative_base()

class List(Base):
    __tablename__ = 'list'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    problems = relationship("Problem", 
                             back_populates="list", 
                             order_by="Problem.position",
                             collection_class=ordering_list("position"))

    def __repr__(self):
        return "list (%s %s)" % (self.id, self.name)

class Problem(Base):
    __tablename__ = 'problem'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    question = Column(Text)
    answer = Column(Text)
    position = Column(Integer)
    listid = Column(Integer, ForeignKey('list.id'))
    list = relationship("List", 
                         back_populates="problems")

    def __repr__(self):
        return "problem (id: %s name: %s position: %s, listid: %s)" % (self.id, 
                                             self.name, 
                                             self.position, 
                                             self.listid)

if os.path.exists("problems_alchemy.db"):
    os.remove("problems_alchemy.db")

engine = create_engine('sqlite:///problems_alchemy.db')
Session = sessionmaker(bind=engine)
session = Session()
Base.metadata.create_all(engine)

# session.add_all([
#     Problem(name="p2", question="q2", answer="a2", listid=1), 
#     Problem(name="p3", question="q3", answer="a3", listid=2), 
#     Problem(name="p5", question="q5", answer="a5", listid=1), 
#     Problem(name="p1", question="q1", answer="a1", listid=1), 
#     Problem(name="p6", question="q6", answer="a6", listid=2), 
#     List(name="l1"), 
#     List(name="l2")]) 
#
# session.commit()

list1 = List(name="list1")
list2 = List(name="list2")
list1.problems.append(Problem(name="p2"))
list2.problems.append(Problem(name="p3"))
list1.problems.append(Problem(name="p4"))
list2.problems.append(Problem(name="p5"))
list1.problems.append(Problem(name="p6"))
list1.problems.pop(1)
session.add(list1)
session.add(list2)

session.commit()

print("before switching")
# for problem in list2.problems:
#    print(problem)
# 4, 5, 6 on list 2
# pop 5
# 4, 6, 
# problem = list2.problems.pop(1)
# 4, 6, 5 
# list2.problems.insert(2, problem)

# session.add(list1)
# session.add(list2)

session.commit()

# print("after switching")
# for problem in list2.problems:
#    print(problem)

# Get all the lists,
# lists = session.query(List)
# list = lists.first()
# print("printing first list")
# print(list)
# print("printing problems on 1st list")
#print(list.problems)
# for problem in list.problems:
#    print(problem)

# Get one list by id
# list = session.query(List).get(2)
# print("printing 2nd list")
# print(list)
# print("printing problems on 2nd list")
#print(list.problems)
# for problem in list.problems:
#    print(problem)

