from pydantic import BaseModel
from typing import Optional
from typing import List as TypingList
from fastapi import Depends, FastAPI, HTTPException
from fastapi.responses import FileResponse
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker, relationship 
from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy import create_engine
from sqlalchemy.ext.orderinglist import ordering_list
from contextlib import contextmanager
import os
import logging

logging.basicConfig(format="%(levelname)s ------- %(message)s")
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

Base = declarative_base()

class List(Base):
    __tablename__ = 'lists'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    problems = relationship("Problem", 
                             back_populates="list", 
                             order_by="Problem.position",
                             collection_class=ordering_list("position"))

    def __repr__(self):
        return "list (%s %s)" % (self.id, self.name)

class Problem(Base):
    __tablename__ = 'problems'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    question = Column(Text)
    answer = Column(Text)
    position = Column(Integer)
    listid = Column(Integer, ForeignKey('lists.id'))
    list = relationship("List", 
                         back_populates="problems")

    def __repr__(self):
        return "problem (%s %s %s %s %s %s)" % (self.id, 
                                             self.name, 
                                             self.question,
                                             self.answer,
                                             self.position, 
                                             self.listid)


engine = create_engine(
    "sqlite:///problems.db", 
    connect_args={"check_same_thread": False}
)

SessionLocal = sessionmaker(bind=engine)
Base.metadata.create_all(bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def db_get_lists(db):
    return db.query(List).all()

def db_get_list(db, id):
    return db.query(List).get(id)

def db_delete_problem_by_index(db, listid, index):
    list = db.query(List).get(listid)
    problem=list.problems.pop(index)
    db.delete(problem)
    db.commit()
    return {}

class SchemaProblemId(BaseModel):
    id: int
    class Config:
        orm_mode = True

class SchemaProblemIn(BaseModel):
    name: str
    question: str
    answer: str
    class Config:
        orm_mode = True


class SchemaProblem(BaseModel):
    id: int
    name: str
    question: str
    answer: str
    position: int 
    listid: int
    class Config:
        orm_mode = True


app = FastAPI()

# Lets serve the SPA here
@app.get("/")
def index():
    return FileResponse("index.html") 

# Then we serve the api here 
@app.get("/api/v1")
def root():
    return {"Hello": "Fastapi"}

@app.get('/api/v1/lists/1/problems/', response_model=TypingList[SchemaProblem])
def get_problems(db: Session=Depends(get_db)): 
    try:
        listid=1
        list = db.query(List).get(listid)
        return list.problems
    except:
        raise HTTPException(status_code=404, 
                detail="Problems not found")

@app.get('/api/v1/problems/{id}')
def get_problem(id: int, db: Session=Depends(get_db)):
    try:
        problem = db.query(Problem).get(id)
        problem.list.problems.remove(problem)
        db.delete(problem)
        db.commit()
        return {}
    except:
        raise HTTPException(status_code=404, 
                detail="Problem not found")

@app.put('/api/v1/problems/{id}')
def update_problem(id: int, problem_in: SchemaProblemIn, db: Session=Depends(get_db)):
    try:
        p = db.query(Problem).get(id)

        p.name = problem.name
        p.question = problem.question
        p.answer = problem.answer

        db.commit()
        return db.query(Problem).get(id)
    except:
        raise HTTPException(status_code=404, 
                detail="Problem update failed")

@app.delete('/api/v1/problems/{id}')
def delete_problem(id: int, db: Session=Depends(get_db)):
    try:
        problem = db.query(Problem).get(id)
        problem.list.problems.remove(problem)
        db.delete(problem)
        db.commit()
        return {}
    except:
        raise HTTPException(status_code=404, 
                detail="Problem not deleted")

@app.post('/api/v1/lists/1/problems/')
def append_problem(problem_in:SchemaProblemIn, db: Session=Depends(get_db)):
    try:
        listid = 1
        p = Problem(name = problem.name,
                    question = problem.question,
                    answer= problem.answer)
        list = db.query(List).get(listid)
        list.problems.append(p)
        db.commit()

        print(p.id)

        # the new id is on p now
        return db.query(Problem).get(p.id)
    except:
        raise HTTPException(status_code=404, 
                detail="Problem creation failed")


@app.post('/api/v1/lists/1/up')
def move_problem_up(problem_id:SchemaProblemId, db: Session=Depends(get_db)):
    try:
        return db_move_problem_up(db, listid=1,problem=problem_in)
    except:
        raise HTTPException(status_code=404, 
                detail="Problem creation failed")

